<?php

namespace AppBundle\Controller\Web\Events;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Entity\Event;
use AppBundle\Form\EventType;

/**
 * @Route("/evenements")
 */
class ManagerController extends Controller
{
    /**
     * @Route("", name="app_events_index")
     * @Template("AppBundle:Web/Events:index.html.twig")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $keyword = $request->query->get('search');
        if(!is_null($keyword)){
            $events = $em->getRepository(Event::class)->researchEvents($keyword);
        } else {
            $events = $em->getRepository(Event::class)->findAll();
        }
 
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $events,
            $request->query->getInt('page', 1),
            5
        );

        return [
            "events" => $pagination,
            "keyword" => $keyword,
        ];
    }

    /**
     * @Route("/{id}/voir", name="app_events_view")
     * @Template("AppBundle:Web/Events:view.html.twig")
     * @ParamConverter("event", class="AppBundle:Event")
     */
    public function viewAction(Request $request, Event $event)
    {
        return [
            "event" => $event,
        ];
    }
}
