<?php

namespace AppBundle\Controller\Api;

use Symfony\Component\HttpFoundation\Request;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;

use FOS\RestBundle\Context\Context;

use AppBundle\Entity\Event;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class EventsController extends FOSRestController
{

    /**
     * @Get("/events")
     * @ApiDoc(
     *  resource=true,
     *  description="This is a description of your API method"
     * )
     */
    public function getEventsAction(Request $request)
    {
    	$em = $this->getDoctrine()->getManager();

    	$limit = $request->query->get("limit", 10);
    	$offset = $request->query->get("offset", 0);

    	$events = $em->getRepository(Event::class)->findBy(
    		[], ["id" => "desc"], $limit, $offset
    	);
		
		$context = new Context();
		$context->addGroup('details');
		$view = $this->view($events, 200);
		$view->setContext($context);

    	return $view;
    }

    /**
     * @Post("/events")
     * @ApiDoc(
     *  description="This is a description of your API method"
     * )
     */
    public function postAction()
    {
    	var_dump("toto");
    	exit;
    }
}
