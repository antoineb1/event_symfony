<?php

namespace AppBundle\Controller\Admin\Events;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Entity\Event;
use AppBundle\Entity\Event\Media;
use AppBundle\Form\EventType;
use AppBundle\Form\Event\MediaType;

class ManagerController extends Controller
{
    /**
     * @Route("/evenements/creer", name="app_events_create")
     * @Template("AppBundle:Admin/Events:create.html.twig")
     */
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $event = new Event();
        $form = $this->createForm(EventType::class, $event);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($event);
            $em->flush();

            $this->addFlash("success", "Vous venez d'ajouter un évènement");

            return $this->redirectToRoute("app_events_index");
        }

        return [
            "form" => $form->createView(),
        ];
    }

    /**
     * @Route("/evenements/{id}/edit", name="app_events_edit")
     * @Template("AppBundle:Admin/Events:edit.html.twig")
     * @ParamConverter("event", class=Event::class)
     */
    public function editAction(Request $request, Event $event)
    {
        $em = $this->getDoctrine()->getManager();

        $media = new Media();

        $eventForm = $this->createForm(EventType::class, $event);
        $mediaForm = $this->createForm(MediaType::class, $media);

        if ($request->isMethod('POST')) {
            if ($request->request->has('event')) {
                $eventForm->handleRequest($request);   
                if ($eventForm->isValid()) {
                    $em->persist($event);
                    $em->flush();
                    
                    $this->addFlash("success", "Vous venez de modifier votre évènement.");
                }
            }

            if ($request->request->has('event_media')) {
                $mediaForm->handleRequest($request);   
                if ($mediaForm->isValid()) {
                    $media->setEvent($event);
                    $em->persist($media);
                    $em->flush();
                    
                    $this->addFlash("success", "Vous venez de modifier votre média.");
                }
            }
        }

        return [
            "form1" => $eventForm->createView(),
            "form2" => $mediaForm->createView(),
        ];
    }
}
